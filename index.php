<?php session_start();?>
<?php  include('user.php');  
       
    $object = new User();

    if(isset($_POST['login'])){  

        $username = $_POST['username'];  
        $password = $_POST['password'];
        $user = $object->login($username, $password);

        if ($user['username'] == $username) {
            $_SESSION['uid'] = $user['id'];
            $_SESSION['user'] = $user['username'];
            $_SESSION['career'] = $user['career'];
            header("Location: home.php");
        } else {
            echo("Wrong Credentials");
        }
    }

    if(isset($_POST['register'])){

        $newusername = $_POST['newusername'];
        $newpassword = $_POST['newpassword'];  
        $newcareer = $_POST['newcareer'];

        $checkUsername = $object->isUsernameExist($newusername);

        if ($checkUsername == 0) {
            $register = $object->register($newusername, $newpassword, $newcareer);
            if($register){  
                 echo "<script>alert('Registration Successful')</script>";  
            }else{  
                echo "<script>alert('Registration Not Successful')</script>";  
            }
        }else{
            echo "<script>alert('Username already exist, try another username')</script>"; 
        }
    }  
?>  
<!DOCTYPE html>  
 <html lang="en" class="no-js">  
 <head>  
        <meta charset="UTF-8" />  
        <title>Login and Registration Form</title>   
    </head>  
    <body>  
        <div class="container">  
              
              
            <header>  
                <h1>Login Form  </h1>  
            </header>  
            <section>               
                <div id="container_demo" >  
                     
                    <a class="hiddenanchor" id="toregister"></a>  
                    <a class="hiddenanchor" id="tologin"></a>  
                    <div id="wrapper">  
                        <div id="login" class="animate form">  
                           <form method="post" action="">  
                                <h1>Log in</h1>   
                                <p>   
                                    <label for="emailsignup" class="youmail" data-icon="e" > Your Username</label>  
                                    <input id="emailsignup" name="username" required="required" type="text" placeholder="Enter your username"/>   
                                </p>  
                                <p>   
                                    <label for="password" class="youpasswd" data-icon="p"> Your password </label>  
                                    <input id="password" name="password" required="required" type="password" placeholder="eg. X8df!90EO" />   
                                </p> 
                                <p class="login button">   
                                    <input type="submit" name="login" value="Login" />   
                                </p>  
                                <p class="change_link">  
                                    Not a member yet ?  
                                    <a href="#toregister" class="to_register">Join us</a>  
                                </p>  
                            </form>  
                        </div>  
                        <br>
                        <header>  
                            <h1>Registration Form  </h1>  
                        </header> 
                        <div id="register" class="animate form">  
                            <form method="post" action="">  
                                <h1> Sign up </h1>   
                                <p>   
                                    <label for="usernamesignup" class="uname" data-icon="u">Your username</label>  
                                    <input id="usernamesignup" name="newusername" required="required" type="text" placeholder="Your username" />  
                                </p>  
                                <p>   
                                    <label for="emailsignup" class="youmail" data-icon="e" > Your Password</label>  
                                    <input id="emailsignup" name="newpassword" required="required" type="password" placeholder="Your passowrd"/>   
                                </p>  
                                <p>   
                                    <label for="passwordsignup" class="youpasswd" data-icon="p">Your career </label>  
                                    <input id="passwordsignup" name="newcareer" required="required" type="text" placeholder="Your career"/>  
                                </p>  
                                <p class="signin button">   
                                    <input type="submit" name="register" value="Sign up"/>   
                                </p>  
                                <p class="change_link">    
                                    Already a member ?  
                                    <a href="#tologin" class="to_register"> Go and log in </a>  
                                </p>  
                            </form>  
                        </div>    
                    </div>  
                </div>    
            </section>  
        </div>  
    </body>  
</html>  