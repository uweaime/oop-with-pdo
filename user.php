<?php

class User {
    
    public $db;

    public function __construct(){
        try{
            $this->db = new PDO('mysql:host=localhost;dbname=php_oop;charset=utf8', 'root', '');
            // echo "Success";
        }
        catch(Exception $e){
            // echo $e->getMessage();
            echo "Oops";
        }
        
    }

    public function isUsernameExist($username){

        $check = $this->db->prepare("SELECT username FROM developers WHERE username=:username");
        $check->bindValue(':username', $username, PDO::PARAM_STR);
        $check->execute();
        $countRow = $check->rowCount();

        return $countRow;
    }
    
    public function register($username,$password,$career){

        $password = sha1($password);
        $register = $this->db->prepare("INSERT INTO developers (username, password, career) VALUES (:username, :password, :career)");
        $register->bindValue(':username', $username, PDO::PARAM_STR);
        $register->bindValue(':password', $password, PDO::PARAM_STR);
        $register->bindValue(':career', $career, PDO::PARAM_STR);
        $register->execute();

        return $register;

    }

    public function login($username, $password){

        $password = sha1($password);       

        $login = $this->db->prepare("SELECT * FROM developers WHERE username=:username AND password=:password");
        $login->bindValue(':username', $username, PDO::PARAM_STR);
        $login->bindValue(':password', $password, PDO::PARAM_STR);
        $login->execute();
        $count = $login->rowCount();

            if ($count == 1) {
                $dataRows = $login->fetch(PDO::FETCH_ASSOC);
                
            return $dataRows;
        }
    }

    public function logout(){
        session_destroy();
        unset($_SESSION['user']);
        header("Location: index.php");
    }
}
 